//console.log('Hellow Wurld?');

// Get Post Data
fetch('https://jsonplaceholder.typicode.com/posts')
	.then((response) => response.json())
	.then((data) => showPosts(data));

// Add Post Data

document.querySelector('#form-add-post').addEventListener('submit', (e) => {

	// Prevents the page from reloading. Also prevents the default behavior of our event.
	e.preventDefault();

	fetch('https://jsonplaceholder.typicode.com/posts', {
		method: 'POST',
		body: JSON.stringify({
			title: document.querySelector('#txt-title').value,
			body: document.querySelector('#txt-body').value,
			userId: 1
		}),
		headers: { 'Content-type': 'application/json; charset=UTF-8'}
	})
	.then((response) => response.json())
	.then((data) => {
		console.log(data);
		alert('Succesfully added!');

		document.querySelector('#txt-title').value = null;
		document.querySelector('#txt-body').value = '';
	});
});


// Show Posts

const showPosts = (posts) => {
	let postEntries = '';

	posts.forEach((post) => {
		postEntries += `
			<div id="post-${post.id}">
				<h3 id="post-title-${post.id}">${post.title}</h3>
				<p id="post-body-${post.id}">${post.body}</p>
				<button onclick="editPost('${post.id}')">Edit</button> 
				<button onclick="deletePost('${post.id}')">Delete</button> 
			</div>
		`;

		document.querySelector('#div-post-entries').innerHTML = postEntries;
	});
};

// Edit Post. 
const editPost = (id) => {
	let title = document.querySelector(`#txt-edit-${id}`).innerHTML;
	let body = document.querySelector(`#post-body-${id}`).innerHTML;

	document.querySelector('')
}

const deletePost = (id) => {
	// console.log(id);
	// 	for(let i = 0; i < posts.length; i++){

	// 	if(posts[i].id.toString() === id){
	// 		const m = posts.splice(i,1);
	// 		break;
	// 	};
	// };

	fetch(`https://jsonplaceholder.typicode.com/posts`).then((response) => response.json()).then((data) => {

		data.splice(id-1,1);

		document.querySelector(`#post-${id}`).remove();
	})

	
	

	// alert('Succesfully deleted')
	showPosts(posts);
	console.log(posts);
};
