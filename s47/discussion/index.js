// The "document" refers to the whole webpage.
// The "querySelector" is used to select a specific obeject (HTML element) from the document. (webpage)

const txtFirstName = document.querySelector('#txt-first-name');
const spanFullName = document.querySelector('#span-full-name');

txtFirstName.addEventListener('keyup', (event) => {
	spanFullName.innerHTML = txtFirstName.value;
});

txtFirstName.addEventListener('keyup', (event) => {
	// console.log("The button is already released")
	console.log(event);
	// The "event.target" contains the element where the event happened
	console.log(event.target);
	// The "event.target.value" gets the value of teh input object(similar to the textFirstName.value);
	console.log(event.target.value);
});